FROM debian:stretch-slim

#set timezone
RUN ln -snf /usr/share/zoneinfo/Europe/Berlin /etc/localtime && echo "Europe/Berlin" > /etc/timezone

RUN apt-get update
RUN apt-get install -y autoconf automake build-essential pkgconf libtool libzip-dev libjpeg-dev git libavformat-dev libavcodec-dev libavutil-dev libswscale-dev libavdevice-dev libwebp-dev libmicrohttpd-dev

RUN git clone https://github.com/Motion-Project/motion.git 

WORKDIR motion

RUN autoreconf -fiv 
RUN ./configure 
RUN make
RUN make install

COPY ./motion.conf /usr/local/etc/motion/motion.conf

EXPOSE 8080
EXPOSE 8081

RUN apt-get install curl -y
COPY ./on_motion /on_motion
RUN chmod +x /on_motion

CMD ["motion"]