# Motion in Docker for ARM

This project builds motion for ARM and explains how to run it on a pi. You will need to setup a fresh PI with Raspbian Lite like outlined in the setup below. 

## Fresh Raspbian Lite Setup

- Change Password
- sudo raspi-config and: hostname/timezone/enable i2c/enable spi/enable camera/enable
- reboot
- camera (see below)
- cgroup_memory (see below)
- reboot
- curl -sSL https://get.docker.com | sh
- reboot


### Enabling the PI camera /dev/video0 for motion

```
echo "bcm2835-v4l2" | tee -a /etc/modules
```

### Enabling cgroup_memory for Docker

```
Edit the /boot/cmdline.txt and add/change the following parameters

cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory
```

## Configuration
If you run the container with the embedded configuration, you cannot change the motion configuration such as the threshold. It is recommended that you copy the motion.conf and change the values. Then link it into the contianer via a volume as shown below (Example with PGM Mask and custom config)

- motion.conf - this is the motion configuration file. You will most likely want to change the `threshold` value. It defines the number of pixels that trigger motion (and therefore the recordings)
- on_event_start can be configured to trigger a local script, see below.  This script is part of the container and will try to trigger sensorita at `http://sensorita:8080/motion` with a POST request. This means you get information about motion triggers into the sensorita web ui and if you have setup the prometheus metrics system, it will be captured in prometheys time series database. 
```
# Command to be executed when an event starts. (default: none)
# An event starts at first motion detected after a period of no motion defined by event_gap
on_event_start /on_motion
```

- masks: You can use a mask to stop recording if motion is detected in certain areas. I've used `gimp` to create a PGM file - essentially a black and white bitmap file. All black areas do not trigger motion, all white areas trigger motion. 

## Docker 

If you run motion together with `sensorita`, please first create a docker network. Motion will call a HTTP trigger whenever it detects motion and therefore both containers need to be able to talk to each other. 

```
docker network create sensorita
```

### embedded conf


```docker container run -d --network sensorita --device=/dev/video0 -v /home/pi/motionout:/tmp/motion -p 8081:8081 --restart always --name motion registry.gitlab.com/haiges/motion:0.5.0-arm```


### Example conf with PGM Mask and custom config

```docker container run -d --network sensorita --device=/dev/video0 -v /home/pi/motionout:/tmp/motion -v /home/pi/motion/motion_gecko.conf:/usr/local/etc/motion/motion.conf -v /home/pi/motion/mask_gecko.pgm:/mask.pgm -p 8081:8081 --restart always --name motion registry.gitlab.com/haiges/motion:0.5.0-arm
```

